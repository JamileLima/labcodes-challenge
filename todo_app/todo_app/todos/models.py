from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Todo(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(User, related_name="todos",
                              on_delete=models.CASCADE, null=True)
    due_date = models.DateTimeField(null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        return self.title
