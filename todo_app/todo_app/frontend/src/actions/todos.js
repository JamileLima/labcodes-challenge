let headers = { 'Content-Type': 'application/json' };

export const getTodos = () => {
  return (dispatch, getState) => {

    let { token } = getState().auth;

    if (token) {
      headers["Authorization"] = `Token ${token}`;
    }

    return fetch("/api/todos/", { headers, })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 200) {
          return dispatch({ type: 'GET_TODOS', todos: resp.data });
        } else if (resp.status === 401 || resp.status === 403) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        }
      })
  }
}


export const addTodo = todo => {
  return (dispatch, getState) => {
    let { token } = getState().auth;

    if (token) {
      headers["Authorization"] = `Token ${token}`;
    }

    let body = JSON.stringify(todo);

    return fetch("/api/todos/", { headers, method: "POST", body })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 201) {
          return dispatch({ type: 'ADD_TODO', todo: resp.data });
        } else if (resp.status === 401 || resp.status === 403) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        }
      })
  }
}


export const editTodo = (id, todo) => {

  return (dispatch, getState) => {

    let { token } = getState().auth;

    if (token) {
      headers["Authorization"] = `Token ${token}`;
    }

    let body = JSON.stringify(todo);

    return fetch(`/api/todos/${id}/`, { headers, method: "PUT", body })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 200) {
          return dispatch({ type: 'EDIT_TODO', todo: resp.data, id });
        } else if (resp.status === 401 || resp.status === 403) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        }
      })
  }
}

export const deleteTodo = id => {
  return (dispatch, getState) => {

    let { token } = getState().auth;

    if (token) {
      headers["Authorization"] = `Token ${token}`;
    }

    return fetch(`/api/todos/${id}/`, { headers, method: "DELETE" })
      .then(resp => {
        if (resp.status === 204) {
          return { status: resp.status, data: {} };
        } else if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 204) {
          return dispatch({ type: 'DELETE_TODO', id });
        } else if (resp.status === 401 || resp.status === 403) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        }
      })
  }
}
