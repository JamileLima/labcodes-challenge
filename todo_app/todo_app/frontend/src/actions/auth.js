let headers = {
  'Content-Type': 'application/json',
};

export const loginUser = () => {
  return (dispatch, getState) => {
    dispatch({ type: "USER_LOADING" });

    const token = getState().auth.token;

    if (token) {
      headers["Authorization"] = `Token ${token}`;
    }

    return fetch("/api/auth/user/", { headers })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 200) {
          dispatch({ type: 'USER_LOADED', user: resp.data });
          return resp.data;
        } else if (resp.status >= 400 && resp.status < 500) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        }
      })
  }
}

export const login = (username, password) => {
  return (dispatch, getState) => {
    let body = JSON.stringify({ username, password });

    return fetch("/api/auth/login/", { headers, body, method: "POST" })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 200) {
          dispatch({ type: 'LOGIN_SUCCESS', data: resp.data });
          return resp.data;
        } else if (resp.status === 403 || resp.status === 401) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        } else {
          dispatch({ type: "LOGIN_FAILED", data: resp.data });
          throw resp.data;
        }
      })
  }
}


// Register user action

export const registerUser = (username, password) => {
  return (dispatch, getState) => {

    let body = JSON.stringify({ username, password });

    return fetch("/api/auth/register/", { headers, body, method: "POST" })
      .then(resp => {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          });
        } else {
          console.log('====================================');
          console.log('Server error... :(');
          console.log('====================================');
          throw resp;
        }
      })
      .then(resp => {
        if (resp.status === 200) {
          dispatch({ type: 'REGISTRATION_SUCCESS', data: resp.data });
          return resp.data;
        } else if (resp.status === 403 || resp.status === 401) {
          dispatch({ type: "AUTHENTICATION_ERROR", data: resp.data });
          throw resp.data;
        } else {
          dispatch({ type: "REGISTRATION_FAILED", data: resp.data });
          throw resp.data;
        }
      })
  }
}


// Logout user actions
export const logoutUser = () => {
  return (dispatch, getState) => {
    return fetch('/api/auth/logout/', { headers, body: '', method: 'POST' }).then(resp => {
      if (resp.status === 204) {
        return { status: resp.status, data: {} };
      } else {
        if (resp.status < 500) {
          return resp.json().then(data => {
            return { status: resp.status, data };
          })
        } else {
          console.log('====================================');
          console.log('Server error :(');
          console.log('====================================');
          throw resp;
        }
      }
    }).then(resp => {
      if (resp.status === 204) {
        dispatch({ type: "LOGOUT_SUCCESSFUL" });
        return resp.data;
      } else {
        if (resp.status === 403 || resp.status === 401) {
          dispatch({ type: 'AUTHENTICATION_ERROR', data: resp.data });
          throw resp.data;
        }
      }
    })
  }
}