const initialState = [];

export default function todos(state = initialState, action) {
  switch (action.type) {
    case 'GET_TODOS':
      return [...state, ...action.todos];
    case 'ADD_TODO':
      return [...state, action.todo];
    case 'EDIT_TODO': {
      return state.map(todo => todo.id === action.todo.id ?
        Object.assign({}, todo, action.todo) : todo);
    }
    case 'DELETE_TODO':
      return state.filter(todo => todo.id !== action.id);
    default:
      return state;
  }
}
