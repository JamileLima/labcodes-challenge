import { combineReducers } from 'redux';

import todos from './todos';
import auth from './auth';

const todosApp = combineReducers({
  todos,
  auth,
});

export default todosApp;
