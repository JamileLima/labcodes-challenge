/* Core */
import React, { Component } from 'react';
import { connect } from 'react-redux';

/* Actions */
import { todos, auth } from '../actions';

/* Components */
import Header from '../components/Header';
import TodoItem from '../components/TodoItem';

/* Helpers */
import { Redirect } from 'react-router-dom'; 
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import renderIf from '../helpers/renderIf';


export class TodoApp extends Component {

  state = {
    todo: {
      id: null,
      title: '',
      due_date: '',
      is_done: false,
    },
    updateTodoId: null,
  }


  componentDidMount() {
    this.props.getTodos();
  }

  resetTodoForm = () => {
    this.setState({
      todo: {
        title: '',
        due_date: '',
        is_done: false,
      },
      updateTodoId: null,
    });
  }

  chooseToEdit = (todo) => {

    this.setState({
      todo: {
        id: todo.id,
        title: todo.title,
        due_date: moment(todo.due_date),
        is_done: todo.is_done,
      },
      updateTodoId: todo.id,
    })
  }

  addTodo = (e) => {
    e.preventDefault();
    if (this.state.updateTodoId === null) {
      this.props.addTodo(this.state.todo)
        .then(this.resetTodoForm());
    }
    else {
      this.props.editTodo(this.state.updateTodoId, this.state.todo);
      this.resetTodoForm();
    }
  }

  deleteTodo = (id) => {
    this.props.deleteTodo(id);
  }

  markTodoDone = (todo) => {
    this.props.editTodo(todo.id, { ...todo, is_done: !todo.is_done });
  }

  onInputChange = (e) => {
    this.setState({
      todo: {
        title: e.target.value,
        due_date: this.state.todo.due_date,
      }
    });
  }

  onDateChange = (e) => {
    const due_date = e;
    this.setState({
      todo: {
        id: this.state.todo.id,
        title: this.state.todo.title,
        is_done: this.state.todo.is_done,
        due_date,
      }
    });
  }

  getDate = () => {
    return this.state.todo.due_date;
  }

  onPressLogout = () => {
    this.props.logout();
    // @TODO: Improve this function to fix the problem with firefox
    setTimeout(() => {
      window.location.reload();
    }, 100);
    return (
      <Redirect to="/login" />
    );
  }


  render() {
    return (
      <div className="todo-app-container">

        <Header userName={this.props.userName} onPressLogout={this.onPressLogout} />

        <form onSubmit={this.addTodo} className="form-container">
          <input
            className="input-todo"
            value={this.state.todo.title}
            placeholder="What needs to be done?"
            onChange={this.onInputChange}
            maxLength="20"
            required
          />

          <DatePicker
            className="date-picker"
            dateFormat="DD/MM/YYYY"
            selected={this.getDate()}
            onChange={this.onDateChange}
            placeholderText="When?"
            required
          />

          <div className="btn-container">
            <input className="btn-submit" type="submit" value="Add" />

            <button className="btn-clear" onClick={this.resetTodoForm}>Clear</button>
          </div>
        </form>

        {
          this.props.todos.map((todo, id) => {

            return (
              <div key={id} className="todo-list-container">
                <table>
                  <tbody>
                    <tr>

                      <TodoItem todo={todo} />

                      <td>
                        <button
                          onClick={() => this.markTodoDone(todo)}
                          className="btn-mark-to-done">
                          {
                            todo.is_done ? 'Undone' : 'Done'
                          }
                        </button>
                      </td>

                      <td>
                        <button
                          onClick={() => this.chooseToEdit(todo)}
                          className="btn-edit">
                          Edit
                        </button>
                      </td>

                      <td>
                        <button
                          onClick={() => this.deleteTodo(todo.id)}
                          className="btn-delete">
                          Delete
                        </button>
                      </td>

                    </tr>
                  </tbody>
                </table>
              </div>
            )
          })
        }

        {
          renderIf(this.props.todos.length <= 0,
            <p className="first-task-message"> Hey, create a task! :) </p>
          )
        }

      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    todos: state.todos,
    userName: state.auth.user.username,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getTodos: () => {
      dispatch(todos.getTodos())
    },
    addTodo: (todo) => {
      return dispatch(todos.addTodo(todo));
    },
    editTodo: (id, todo) => {
      return dispatch(todos.editTodo(id, todo));
    },
    deleteTodo: (id) => {
      dispatch(todos.deleteTodo(id));
    },
    logout: () => dispatch(auth.logoutUser()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoApp);

