import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => (
  <div className="not-found-container">
    <h1 className="not-found-text">Page not found :(</h1>
    <p>
      Click <Link className="link-page-not-found" to="/"> here </Link> to return :)
      </p>
  </div>
);

export default NotFound;
