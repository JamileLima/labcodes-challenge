import React from 'react';

import moment from 'moment';

const TodoItem = (props) => {
  return (
    <td className={props.todo.is_done ? "todo-not-completed" : "todo-completed"}>
      {props.todo.title} - {moment(props.todo.due_date).format('DD/MM/YYYY')}
    </td>
  )
};

export default TodoItem;
