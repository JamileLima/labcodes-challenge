/* Core */
import React, { Component } from 'react';

/* Redux and Actions */
import { connect } from 'react-redux';
import { auth } from '../actions';

/* Helpers */
import { Link, Redirect } from 'react-router-dom';
import renderIf from '../helpers/renderIf';

class Login extends Component {
  state = {
    username: '',
    password: '',
  }

  submit = (e) => {
    e.preventDefault();
    this.props.login(this.state.username, this.state.password);
  }

  onInputUserNameChange = (e) => {
    this.setState({
      username: e.target.value
    });
  }

  onInputPasswordChange = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />
    }

    return (
      <div>
        <form onSubmit={this.submit}>
          <fieldset className="form-container">

            <legend className="login-legend">Login</legend>

            <div className="form-items-container">

              <label htmlFor="username" className="username-label">Username</label>
              <input
                type="text"
                id="username"
                onChange={this.onInputUserNameChange}
              />

              <label htmlFor="password" className="password-label">Password</label>
              <input
                type="password"
                id="password"
                onChange={this.onInputPasswordChange}
              />

              <button type="submit" className="btn-submit-form">Submit</button>

              <p className="link-to-register-text">
                You don't have account?
              <Link to="/register" className="link-to-register"> Register </Link>
              </p>

            </div>

          </fieldset>
        </form>
        {
          renderIf(this.props.errors.length > 0,
            (
              <ul className="error-message-container">
                {this.props.errors.map((error) => (
                  <li key={error.field} className="error-message">
                    {error.message}
                  </li>
                ))}
              </ul>
            )
          )
        }

      </div>
    )
  }
}

const mapStateToProps = state => {
  let errors = [];
  if (state.auth.errors) {
    errors = Object.keys(state.auth.errors).map(field => {
      if (field === "detail")
        return { field, message: 'Digite seu usuário e senha :)' };
      return { field, message: state.auth.errors[field] };
    });
  }
  return {
    errors,
    isAuthenticated: state.auth.isAuthenticated,
  }
}


const mapDispatchToProps = dispatch => {
  return {
    login: (username, password) => {
      return dispatch(auth.login(username, password));
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
