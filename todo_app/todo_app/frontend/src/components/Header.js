import React from 'react';
const Header = (props) => {

  const onPressLogout = () => { props.onPressLogout(); }

  return (
    <div className="header-container" >
      <h1 className="header-text">Hello, {props.userName}</h1>
      <button onClick={onPressLogout} className="btn-logout">Logout</button>
    </div>
  );
};

export default Header;
