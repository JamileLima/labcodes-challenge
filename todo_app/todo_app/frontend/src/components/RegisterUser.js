/* Core */
import React, { Component } from 'react';

/* Redux */
import { connect } from 'react-redux';

/* Helpers */
import { Link, Redirect } from 'react-router-dom';
import renderIf from '../helpers/renderIf';

/* Actions */
import { auth } from '../actions';

class RegisterUser extends Component {
  state = {
    username: '',
    password: '',
  }

  submit = e => {
    e.preventDefault();
    this.props.registerUser(this.state.username, this.state.password);
  }

  inputRegisterUserNameChange = (e) => {
    this.setState({
      username: e.target.value,
    });
  }

  inputRegisterPasswordChange = (e) => {
    this.setState({
      password: e.target.value,
    });
  }

  render() {
    if (this.props.isAuthenticated) {
      return (
        <Redirect to="/" />
      )
    }

    return (
      <div>
        <form onSubmit={this.submit}>
          <fieldset>

            <legend className="register-user-legend">
              Create account
          </legend>

            <div className="register-user-items-container">
              <label htmlFor="username" className="username">
                Username
            </label>
              <input
                type="text"
                id="username"
                onChange={this.inputRegisterUserNameChange}
              />

              <label htmlFor="password" className="password">
                Password
            </label>
              <input
                type="password"
                id="password"
                onChange={this.inputRegisterPasswordChange}
              />

              <button type="submit" className="btn-submit-form">
                Register
            </button>

              <p className="link-to-register-text">
                Already have an account?
            <Link to="/login" className="link-to-register"> Login </Link>
              </p>

            </div>


          </fieldset>
        </form>

        {
          renderIf(this.props.errors.length > 0,
            (
              <ul className="error-message-container">
                {this.props.errors.map((error) => (
                  <li key={error.field} className="error-message">
                    {error.message}
                  </li>
                ))}
              </ul>
            )
          )
        }
      </div>
    )
  }
}

const mapStateToProps = state => {
  let errors = [];
  if (state.auth.errors) {
    errors = Object.keys(state.auth.errors).map(field => {
      if (field === "detail")
        return { field, message: '' };
      return { field, message: state.auth.errors[field] };
    });
  }
  return {
    errors,
    isAuthenticated: state.auth.isAuthenticated
  };
}

const mapDispatchToProps = dispatch => {
  return {
    registerUser: (username, password) => dispatch(auth.registerUser(username, password)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterUser);
