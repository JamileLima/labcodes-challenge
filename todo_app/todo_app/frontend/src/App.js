/* Core */
import React, { Component } from 'react';
import { Route, Switch, BrowserRouter, Redirect } from 'react-router-dom';

/* Redux */
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

/* Reducers and Actions*/
import todosApp from './reducers';
import { auth } from './actions'

/* Components */
import RegisterUser from './components/RegisterUser';
import Login from './components/Login';
import TodoApp from './containers/TodoApp'
import NotFound from './components/NotFound';

let store = createStore(todosApp, applyMiddleware(thunk));

class RootContainerComponent extends Component {

  componentDidMount() {
    this.props.loginUser();
  }

  PrivateRoute = ({ component: ChildComponent, ...rest }) => {
    return <Route {...rest} render={props => {
      if (this.props.auth.isLoading) {
        return (<em>Loading...</em>);
      } else if (!this.props.auth.isAuthenticated) {
        return (<Redirect to="/login" />);
      } else {
        return (<ChildComponent {...props} />);
      }
    }} />
  }


  render() {

    let { PrivateRoute } = this;

    /* PRIVATE ROUTE - USER NOT AUTHENTICATED */

    return (
      <BrowserRouter>
        <Switch>
          <PrivateRoute exact path="/" component={TodoApp} />
          <Route exact path="/register" component={RegisterUser} />
          <Route exact path="/login" component={Login} />
          <Route component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}


const mapStateToProps = state => {
  return {
    auth: state.auth,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loginUser: () => {
      return dispatch(auth.loginUser());
    }
  }
}

/* ROOT CONTAINER - USER AUTHENTICATED */

let RootContainer = connect(mapStateToProps, mapDispatchToProps)(RootContainerComponent);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    )
  }
}
