# Labcodes Challenge - TodoApp :memo:

Simple todo app with React + Django :zap:

## How to setup :wrench:

* Create a virtual env with Python3.6. Click [here](https://docs.python.org/3/tutorial/venv.html) to get more information.
* Clone the repo
    * `$ git clone git@github.com:jamilelima/labcodes-challenge.git`
* `$ cd labcodes-challenge/todo_app/todo_app`
* `$ pip install -r requirements.txt`
   * Please make sure if your version of pip is >= `10.0.1` running `pip --version`
* `$ ./manage.py migrate`
* `$ ./manage.py runserver`
* `$ cd frontend`
* `$ yarn install`
* `$ yarn run start`
* Access `localhost:8000` on your browser and enjoy :tada:

